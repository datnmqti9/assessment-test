import React from 'react'
import { AddNewItem } from './AddNewItem'
import { ColumnContainer, ColumnTitle } from './styles'
import { useDispatch } from "react-redux";
import { generateUUID } from './helpers';
import { Task } from "./listsReducer";

interface IColumnProps {
  listID: number | string
  text?: string
}

export const Column = ({
  listID,
  text,
  children,
}: React.PropsWithChildren<IColumnProps>) => {
  const dispatch = useDispatch();

  const handleAddTask = (task: Task) => {
    if (!task.name) return;
    dispatch({ type: "ADD_TASK", payload: task });
  };

  return (
    <ColumnContainer>
      <ColumnTitle>{text}</ColumnTitle>
      {children}
      <AddNewItem
        dark
        toggleButtonText="+ Add another task"
        onAdd={(value) => handleAddTask({listID, id: generateUUID(), name: value})}
      ></AddNewItem>
    </ColumnContainer>
  )
}
