import { createStore } from "redux";
import { listsReducer } from "./listsReducer";

export const store = createStore(listsReducer as any);