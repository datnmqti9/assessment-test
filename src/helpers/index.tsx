export function generateUUID() {
  const uuid = Math.random().toString(16).slice(2);
  return `${Date.now()}${uuid}`;
}
