export interface Task {
  listID?: number | string
  id: number | string;
  name: string;
}

export interface List {
  id: number | string;
  name: string;
  tasks: Array<Task>;
}

export interface ListsState {
  lists: List[];
}

const initialState = {
  // TODO: Organize key object style for easy handling
  lists: [
    {
      id: 1,
      name: 'To Do',
      tasks: [
        {
          id: 11,
          name: 'Fixed value'
        }
      ]
    },
    {
      id: 2,
      name: 'In Progress',
      tasks: []
    },
    {
      id: 3,
      name: 'Done',
      tasks: []
    },
  ],
};

export type Action = { type: "ADD_TASK" | "ADD_LIST"; payload: object };

export const listsReducer = (
  state: ListsState = initialState,
  action: Action
) => {
  switch (action.type) {
    case "ADD_TASK": {
      const { listID, id, name }: any = action.payload;
      const listIndex = state.lists.findIndex(list => list.id === listID)
      const list = {...state.lists[listIndex]}
      list.tasks = [...list.tasks, {id, name}]
      
      const newLists = state.lists;
      newLists[listIndex] = list;

      return { ...state, lists: [...newLists]};
    }

    case "ADD_LIST": {
      const { id, name }: any = action.payload;
      
      return { ...state, lists: [...state.lists, {
        id,
        name,
        tasks: []
      }]};
    }
    default:
      return state;
  }
};