import { AddNewItem } from './AddNewItem'
import './App.css'
import { Card } from './Card'
import { Column } from './Column'
import { AppContainer } from './styles'


import { useSelector, useDispatch } from "react-redux";
import { ListsState, List } from "./listsReducer";
import { generateUUID } from './helpers'

const App = () => {
  const lists = useSelector<ListsState, ListsState["lists"]>(
    (state) => state.lists
  );

  const dispatch = useDispatch();

  const handleAddList = (list: List) => {
    if (!list.name) return;
    dispatch({ type: "ADD_LIST", payload: list });
  };

  return (
    <AppContainer>
      {lists.map((list) => {
        return (<Column key={list.id} listID={list.id} text={list.name}>
          {list.tasks.map((task) => <Card key={task.id} text={task.name} />)}
        </Column>);
      })}

      <AddNewItem
        toggleButtonText="+ Add another list"
        onAdd={(value) => handleAddList({ id: generateUUID(), name: value, tasks: [] })}
      ></AddNewItem>
    </AppContainer>
  )
}

export default App